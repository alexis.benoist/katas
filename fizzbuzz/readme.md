# FizzBuzz

You should implements a function which implements the following rules.


## Rules

Any number divisible by 3 is replace by word `Fizz` and any divisible by 5 by the word `Buzz`.
Number divisible by both become `FizzBuzz`.


## Examples

    1 => 1
    2 => 2
    3 => Fizz
    4 => 4
    5 => Buzz
    6 => Fizz
    15 => FizzBuzz


# Possibles constraints

- Try open/close principle
- Try a functional approch
- Try to implement a monoid
