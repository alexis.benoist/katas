# Katas

This is a collection of code katas, dojo and other training exercies.


## Learning TDD

- fizzbuzz
- foobarqix
- lear-year
- one-two
- rpn-calculator
- string-calculator
- diamond
- prime-factors
- coin-changer
- mars-rover
- potter
- reversi
- trading-card-game


## Double loop (TDD + Acceptance test)

- gameoflife
- bowling-game


## Refactoring

- crc-refactoring
- movie-rental
- glided-rose
- task-list
- katastrophic
- quote-bot
- trip-service
- tire-pressure-monitoring-system
- text-converter
- telemtry-system
- turn-ticker-dispenser
- space
- simwar


## Slicing

- carpaccio


## Conception

- digisim
- birthday-greetings
- basket-pricer
- bank-account
- coffe-machine-project
- coffe-machine
- wallet
