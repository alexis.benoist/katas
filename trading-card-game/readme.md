# Trading Card Game

## About this Kata


In this Kata you will be implementing a rudimentary two-player trading card game. The rules are loosely based on Blizzard Hearthstone (<http://us.battle.net/hearthstone/en/>) which itself is an already much simpler and straight-forward game compared to other TCGs like *Magic: The Gathering* (<http://www.wizards.com/magic/>).


## Problem Description

### Preparation

- Each player starts the game with 30 Health and 0 Mana slots
- Each player starts with a deck of 20 Damage cards with the following Mana costs: `0,0,1,1,2,2,2,3,3,3,3,4,4,4,5,5,6,6,7,8`
- From the deck each player receives 3 random cards has his initial hand


### Gameplay

1. The active player receives 1 Mana slot up to a maximum of 10 total slots
2. The active player's empty Mana slots are refilled
3. The active player draws a random card from his deck
4. The active player can play as many cards as he can afford. Any played card empties Mana slots and deals immediate damage to the opponent player equal to its Mana cost.
5. If the opponent player's Health drops to or below zero the active player wins the game
6. If the active player can't (by either having no cards left in his hand or lacking sufficient Mana to pay for any hand card) or simply doesn't want to play another card, the opponent player becomes active


### Special Rules

- Bleeding Out: If a player's card deck is empty before the game is over he receives 1 damage instead of drawing a card when it's his turn.
- Overload: If a player draws a card that lets his hand size become &gt;5 that card is discarded instead of being put into his hand.
- Dud Card: The 0 Mana cards can be played for free but don't do any damage either. They are just annoyingly taking up space in your hand.


## Advanced Variations

When the normal game rules have become too easy/boring you might consider adding some additional rules like those described below. Some of them will increase the complexity of the game significantly, so you might not want to use all extra rules at once:

### Healing

1. When playing a card the active player can choose to use it for causing damage (see [Basic Gameplay](#BasicGameplay)) or for _healing himself_ by a value equal to the mana cost amount.
2. Players cannot heal up above 30 health.

### Minions

1. Let players choose to play cards either as immediate damage _Attacks_ (same as cards generally worked in the [Basic Gameplay](#BasicGameplay) rules) or as _Minions_ that are put on the board instead. Minions will use the mana cost of their card as Health and Damage value. Playing a 0 card will create a minion with 1 Health and 0 Damage.
2. Health has to be tracked when they receive damage.
3. Each player can have a _maximum of 3 Minions_ on the board at any given time.
4. A Minion will _sleep_ in the turn it was put on the board.
5. In any subsequent turn _each_ Minion can be used _once_ to deal damage to the opponent player or an opponent Minion.
6. A Minion fighting another Minion will result in them dealing their damage value to each other simultaneously.
7. Sleeping Minions will defend themselves in the same way when attacked by another Minion.
8. Players can choose to play an Attack against a Minion. The attacked Minion will not defend itself in this case, thus the attacking player receives no damage from it.
9. When a Minions health drops to or below zero it is removed from the board.


### Miscellaneous

- Let Mana cost and damage dealt be different from each other thus making cheap powerful, expensive mediocre or entirely useless cards possible. This can add a whole new layer of play strategy as some cards might not be desired to be ever played, but eventually have to in order to free your hand for better cards.
- Introduce _Card Drawer_ cards that cost Mana but don't do any damage. Instead they let you draw a given number of cards from your deck. Those cards can be used in the current turn or later on (just as if normally drawn at the beginning of the active player's turn).
- Allow players to create their own decks of 20 cards from a larger _Card Pool_. Let those decks be saved to and loaded from disk before starting a game.


## Clues

When approached iteratively with TDD you can take different starting points, like the player state or the game loop. It is your own choice whether you implement the game for human or computer players - or both.

Game visualization can be anything between System.out and a GUI. You can also increase the difficulty by adding more rules, like Healing cards, Damage independent from Mana cost or introducing individual Deck building. You will find some examples of Advanced Variations from the
Kata's author at <https://github.com/bkimminich/kata-tcg>. Even without extra rules the toughest part of this Kata might be coming up with actually smart CPU player decision-making algorithms.
