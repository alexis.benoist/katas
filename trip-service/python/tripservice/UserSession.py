from DependendClassCallDuringUnitTestException import DependendClassCallDuringUnitTestException


class UserSession:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not isinstance(cls._instance, cls):
            cls._instance = object.__new__(cls, *args, **kwargs)
        return cls._instance

    @staticmethod
    def get_instance():
        return UserSession()

    def is_user_logged_in(self, user):
        raise DependendClassCallDuringUnitTestException(
            "UserSession.isUserLoggedIn() should not be called in an unit test"
        )

    def get_logged_user(self):
        raise DependendClassCallDuringUnitTestException(
            "UserSession.getLoggedUser() should not be called in an unit test"
        )
