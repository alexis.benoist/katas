# Katastrophic

Le Kata de Refactoring 'Katastrophic' écrit par Eric Lemerdy : https://github.com/ericlemerdy/katastrophic/

## Objectifs

  * Comprendre ce que le code fait.
  * Rendre le code plus lisible sans en changer son comportement.
